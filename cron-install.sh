#!/usr/bin/env bash

_PATH=$(dirname $(realpath $0))

. "$_PATH/config"

if [ -z $SUDO_USER ]; then
	echo -e "#!/usr/bin/bash\n$(realpath backup.sh)" > /etc/cron.daily/backup-$BACKUP_ID
	echo -e "#!/usr/bin/bash\n$(realpath backup-prune.sh)" > /etc/cron.weekly/backup-prune-$BACKUP_ID
else
	echo -e "#!/usr/bin/bash\nsudo -u $SUDO_USER $(realpath backup.sh)" > /etc/cron.daily/backup-$BACKUP_ID
	echo -e "#!/usr/bin/bash\nsudo -u $SUDO_USER $(realpath backup-prune.sh)" > /etc/cron.weekly/backup-prune-$BACKUP_ID
fi

chmod +x \
	/etc/cron.daily/backup-$BACKUP_ID \
	/etc/cron.weekly/backup-prune-$BACKUP_ID

