#!/usr/bin/bash

# Prune old snapshots

_PATH=$(dirname $(realpath $0))

# Needed so the cronjobs will fail if backup returns != 0
set -e

. "$_PATH/config"

# Use this to (de)prioritize the backup
IONICE_PRIORITY=${IONICE_PRIORITY:-7}
NICE_PRIORITY=${NICE_PRIORITY:-19}

[[ $(restic snapshots) ]] || { echo 'No snapshots there. Is repo initialized?'; exit 1; }

ionice -n $IONICE_PRIORITY nice -n $NICE_PRIORITY \
	restic $RESTIC_VERBOSE forget --prune \
	--keep-tag $RESTIC_KEEP_TAG \
	--keep-daily $RESTIC_KEEP_DAILY \
	--keep-weekly $RESTIC_KEEP_WEEKLY \
	--keep-monthly $RESTIC_KEEP_MONTHLY \
	--keep-yearly $RESTIC_KEEP_YEARLY \
	--keep-tag $RESTIC_KEEP_TAG
#   --keep-hourly $RESTIC_KEEP_HOURLY \

ionice -n $IONICE_PRIORITY nice -n $NICE_PRIORITY \
	restic $RESTIC_VERBOSE check
