#!/usr/bin/bash

_PATH=$(dirname $(realpath $0))

# Needed so the cronjobs will fail if backup returns != 0
set -e

. "$_PATH/config"

# Use this to (de)prioritize the backup
IONICE_PRIORITY=${IONICE_PRIORITY:-7}
NICE_PRIORITY=${NICE_PRIORITY:-19}

# IMPROVE: restic snapshots does not return a more granular code to see if the repo is acutally created
[[ $(restic snapshots) ]] || { echo 'No snapshots there. Is repo initialized?'; exit 1; }

ionice -n $IONICE_PRIORITY nice -n $NICE_PRIORITY \
	restic $RESTIC_VERBOSE backup $RESTIC_BACKUP_SRC \
	--one-file-system \
	--exclude-if-present ".nobackup" \
	--exclude-file "$RESTIC_EXCLUDE_FILE"

ionice -n $IONICE_PRIORITY nice -n $NICE_PRIORITY \
	restic $RESTIC_VERBOSE check
