# Restic Backup Helper

## Usage

1. Type secrets on the appropriate files (`aws_secret_access_key`, `aws_access_key` and `restic_password`)
2. Set files to exclude in `excludes`
3. Just run `backup.sh` to backup
4. Periodally run `backup-prune.sh` to forget and prune

## Anacron

Better use `anacron` for laptop/workstation. Run (with sudo) `cron-install.sh` to create simlinks to /etc/cron.{daily/weekly}
